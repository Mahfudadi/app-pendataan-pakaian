package saputro.mahfud.apppendataanpakaian

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class MainActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imUpload ->{
                requestPermissions()
            }
            R.id.btnInsert ->{
                queryInsertUpdateDelete("insert")
            }
            R.id.btnUpdate ->{
                queryInsertUpdateDelete("update")
            }
            R.id.btnDelete ->{
                queryInsertUpdateDelete("delete")
            }
        }
    }

    lateinit var mediaHelper: MediaHelper
    lateinit var pakaianAdapter : AdapterDataPakaian
    lateinit var kategoriAdapter : ArrayAdapter<String>
    var daftarPakaian = mutableListOf<HashMap<String,String>>()
    var daftarKategori = mutableListOf<String>()
    var url = "http://192.168.43.77/app-pendataan-pakaian-web/show_data.php"
    var url2 = "http://192.168.43.77/app-pendataan-pakaian-web/get_nama_kategori.php"
    var url3 = "http://192.168.43.77/app-pendataan-pakaian-web/query_ins_upd_del.php"
    var imStr = ""
    var pilihKategori = ""
    var nmFile = ""
    var fileUri = Uri.parse("")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        pakaianAdapter = AdapterDataPakaian(daftarPakaian,this)
        mediaHelper = MediaHelper(this)
        listPakaian.layoutManager = LinearLayoutManager(this)
        listPakaian.adapter = pakaianAdapter

        kategoriAdapter = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,
            daftarKategori)
        spinKategori.adapter = kategoriAdapter
        spinKategori.onItemSelectedListener = itemSelected

        imUpload.setOnClickListener(this)
        btnInsert.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
        btnFind.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        showDataPakaian("")
        getNamaProdi()
    }

    val itemSelected = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spinKategori.setSelection(0)
            pilihKategori = daftarKategori.get(0)
        }
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihKategori = daftarKategori.get(position)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if(requestCode==mediaHelper.getRcCamera()){
                imStr = mediaHelper.getBitmapToString(imUpload,fileUri)
                nmFile = mediaHelper.getMyFileName()
            }
        }
    }

    fun queryInsertUpdateDelete(mode : String){
        val request = object : StringRequest(Method.POST,url3,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi berhasil",Toast.LENGTH_SHORT).show()
                    showDataPakaian("")
                }else{
                    Toast.makeText(this,"Operasi GAGAL",Toast.LENGTH_SHORT).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server",Toast.LENGTH_SHORT).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                val nmFile = "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
                    .format(Date())+".jpg"
                when(mode){
                    "insert" ->{
                        hm.put("mode","insert")
                        hm.put("id_pakaian",edId.text.toString())
                        hm.put("nm_pakaian",edNamaPakaian.text.toString())
                        hm.put("nm_kategori",pilihKategori)
                        hm.put("image",imStr)
                        hm.put("file",nmFile)
                        hm.put("harga",edHarga.text.toString())
                        hm.put("stok",edStok.text.toString())
                    }
                    "update" ->{
                        hm.put("mode","update")
                        hm.put("id_pakaian",edId.text.toString())
                        hm.put("nm_pakaian",edNamaPakaian.text.toString())
                        hm.put("nm_kategori",pilihKategori)
                        hm.put("image",imStr)
                        hm.put("file",nmFile)
                        hm.put("harga",edHarga.text.toString())
                        hm.put("stok",edStok.text.toString())
                    }
                    "delete" ->{
                        hm.put("mode","delete")
                        hm.put("id_pakaian",edId.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun requestPermissions() = runWithPermissions(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA){
        fileUri = mediaHelper.getOutputMediaFileUri()
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_OUTPUT,fileUri)
        startActivityForResult(intent,mediaHelper.getRcCamera())
    }

    fun getNamaProdi(){
        val request = StringRequest(
            Request.Method.POST,url2,
            Response.Listener { response ->
                daftarKategori.clear()
                val jsonArray = JSONArray(response)
                for (x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarKategori.add(jsonObject.getString("nm_kategori"))
                }
                kategoriAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->  }
        )
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun showDataPakaian(namaPakaian : String){
        val request = object : StringRequest(Request.Method.POST,url,
            Response.Listener { response ->
                daftarPakaian.clear()
                val jsonArray = JSONArray(response)
                for (x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var pakaian = HashMap<String,String>()
                    pakaian.put("id_pakaian",jsonObject.getString("id_pakaian"))
                    pakaian.put("nm_pakaian",jsonObject.getString("nm_pakaian"))
                    pakaian.put("nm_kategori",jsonObject.getString("nm_kategori"))
                    pakaian.put("url",jsonObject.getString("url"))
                    pakaian.put("harga",jsonObject.getString("harga"))
                    pakaian.put("stok",jsonObject.getString("stok"))
                    daftarPakaian.add(pakaian)
                }
                pakaianAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("nm_pakaian",namaPakaian)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

}
